<?php
class Connection
{
    private $user = 'root';
    private $pass = 'root';
    protected $con;

    /**
     * Database Connection constructor.
     */
    public function __construct(){
        try {
            $this->con = new PDO('mysql:host=127.0.0.1;port=3306;dbname=pharmacyDB', $this->user, $this->pass);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}

//echo "\nPDOStatement::errorInfo():\n";
//$arr = $stmt->errorInfo();
//print_r($arr);