<?php
require_once ('Connection.php');

class Users extends Connection
{
    private $userID;
    private $firstName;
    private $lastName;
    private $newRegEmail;
    private $role = "user";
    private $newRegPass;

    /**
     * @desc sets the user data into
     * this class variables
     * @param array $data
     * @since 1.0.0
     */
    public function set($data = array()){
        if (array_key_exists('firstName', $data)) {
            $this->firstName = filter_var($data['firstName'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('lastName', $data)) {
            $this->lastName = filter_var($data['lastName'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('newRegEmail', $data)) {
            $this->newRegEmail = filter_var($data['newRegEmail'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('role', $data)) {
            $this->role = filter_var($data['role'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('newRegPass', $data)) {
            $this->newRegPass = md5(filter_var($data['newRegPass'], FILTER_SANITIZE_STRING));
        }
    }

    /**
     * @desc stores the new users info
     * and register them into the system
     * @return bool
     * @since 1.0.0
     */
    public function store(){
        try{
            $stmt = $this->con->prepare("INSERT INTO `users` (`userID`, `firstName`, `lastName`, `email`, `role`, `pass`) VALUES (:userID, :firstName, :lastName, :newRegEmail, :role, :newRegPass)");
            $result =  $stmt->execute(array(
                ':userID' => $this->userID,
                ':firstName' => $this->firstName,
                ':lastName' => $this->lastName,
                ':newRegEmail' => $this->newRegEmail,
                ':role' => $this->role,
                ':newRegPass' => $this->newRegPass
            ));

            if($result){
                return true;
            } else{
                return false;
            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function emailValidation($email){
        $email = filter_var($email, FILTER_SANITIZE_STRING);
        try{
            $stmt = $this->con->prepare("SELECT `email` FROM `users` WHERE `email`='$email'");
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    /**
     * @desc logs in a user into the system
     * and returns back the user data
     * @param $email
     * @param $pass
     * @return array
     * @since 1.0.0
     */
    public function login($email, $pass){
        try{
            $stmt = $this->con->prepare("SELECT `userID`, `firstName`, `lastName`, `email`, `role` FROM `users` WHERE  `email` = '$email' AND `pass` = '$pass'");
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);
            return $res;
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    /**
     * @desc gets all the registered users
     * and returns back their data
     * @return array
     * @since 2.0.0
     */
    public function getUsers(){
        try{
            $stmt = $this->con->prepare("SELECT `userID`, `firstName`, `lastName`, `email`, `role` FROM `users`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There are some problem in connection: " . $e->getMessage();
        }
    }

    /**
     * @desc deletes a specific user from database
     * @param $userID
     * @return bool
     * @since 3.0.0
     */
    public function delUser($userID){
        try{
            $stmt = $this->con->prepare("DELETE FROM `users` WHERE `userID`='$userID'");
            $res = $stmt->execute();

            if ($res)
                return true;
            else
                return false;
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    /**
     * @desc updates a specific user from database
     * @param $userID
     * @return bool
     * @since 3.0.0
     */
    public function updateUser($userID){
        try{
            $stmt = $this->con->prepare("UPDATE `users` SET `firstName` = :firstName, `lastName` = :lastName, `role` = :role WHERE `userID` = '$userID'");
            $res =  $stmt->execute(array(
                ':firstName' => $this->firstName,
                ':lastName' => $this->lastName,
                ':role' => $this->role
            ));

            if ($res)
                return true;
            else
                return false;
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}