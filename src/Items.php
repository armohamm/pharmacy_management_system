<?php
require_once ('Connection.php');

class Items extends Connection
{
    private $productID;
    private $productName;
    private $productGroup;
    private $productCode;
    private $productPrice;

    /**
     * @desc: This function takes the data from user
     * and assign them into class variables
     * @param array $data
     * @since 2.0.0
     */
    public function set($data = array()){
        if (array_key_exists('productID', $data)) {
            $this->productID = filter_var($data['productID'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('productName', $data)) {
            $this->productName = filter_var($data['productName'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('productCode', $data)) {
            $this->productCode = filter_var($data['productCode'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('productPrice', $data)) {
            $this->productPrice = filter_var($data['productPrice'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('productGroup', $data)) {
            $this->productGroup = filter_var($data['productGroup'], FILTER_SANITIZE_STRING);
        }
    }

    /**
     * desc: This function stores the data into database.
     * @return bool
     * @since 2.0.0
     */
    public function store(){
        try{
            $stmt = $this->con->prepare("INSERT INTO `items` (`productID`, `productName`, `productCode`, `productPrice`, `productGroup`) VALUES (:productID, :productName, :productCode, :productPrice, :productGroup)");
            $result =  $stmt->execute(array(
                ':productID' => $this->productID,
                ':productName' => $this->productName,
                ':productCode' => $this->productCode,
                ':productPrice' => $this->productPrice,
                ':productGroup' => $this->productGroup
            ));

//            var_dump(strlen($this->newRegPass));
            if($result){
                return true;
            } else{
                echo "\nPDOStatement::errorInfo():\n";
                $arr = $stmt->errorInfo();
                print_r($arr);
                return false;
            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    /**
     * @desc gets all the item from database
     * @param $email
     * @param $pass
     * @return array
     * @since 1.0.0
     */
    public function getItems(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `items`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    /**
     * @desc deletes a specific product item from database
     * @param $productID
     * @return bool
     * @since 3.0.0
     */
    public function delItems($productID){
        try{
            $stmt = $this->con->prepare("DELETE FROM `items` WHERE `productID`='$productID'");
            $res = $stmt->execute();

            if ($res)
                return true;
            else
                return false;
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    /**
     * @desc updates a specific product item from database
     * @param $productID
     * @return bool
     * @since 3.0.0
     */
    public function updatePost($productID){
        try{
            $stmt = $this->con->prepare("UPDATE `items` SET `productID` = :productID, `productName` = :productName, `productCode` = :productCode, `productPrice` = :productPrice, `productGroup` = :productGroup WHERE `productID` = '$productID'");
            $res =  $stmt->execute(array(
                ':productID' => $this->productID,
                ':productName' => $this->productName,
                ':productCode' => $this->productCode,
                ':productPrice' => $this->productPrice,
                ':productGroup' => $this->productGroup
            ));

            if ($res)
                return true;
            else
                return false;
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}