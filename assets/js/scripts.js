/**
 * @desc an onClick event listener that triggers with the id of the button tag.
 * @since 1.0.0
 */
$("button").click(function(event){
    const buttonID = '#' + this.id;

    switch (buttonID) {
        case '#nav_login_button':
        case '#sub_login_button':
            displayLoginForm();
            break;
        case '#sub_reg_button':
            subRegBtn();
            break;
        case '#sub_close_button_reg_form':
        case '#sub_close_button_login_form':
            closeLoginRegForm();
            break;
        case '#reg_me_button':
            event.preventDefault();
            storeUser();
            break;
        case '#sign_me_button':
            event.preventDefault();
            login();
            break;
        case '#nav_logout_button':
            event.preventDefault();
            window.location.assign("view/admin/userLogout.php");
            break;
        default:
            console.log('Invalid Button ID');
            break;
    }
});

/**
 * @desc an onClick event listener that
 * triggers with the id of the anchor 'a' tag.
 * @since 2.0.0
 */
$("a").click(function(event){
    const buttonID = '#' + this.id;

    switch (buttonID) {
        case '#add_items_btn':
            event.preventDefault();
            displayAddItemsForm();
            break;
        case '#sub_close_button_add_item_form':
            closeAddItemsForm();
            break;
        case '#add_item_btn':
            storeItems();
            break;
        case '#view_items':
            loadItems();
            break;
        case '#view_users':
            loadUsers();
            break;
        default:
            console.log('Invalid Button ID');
            break;
    }
});

/**
 * @desc stores new registered users into database
 * @since 2.0.0
 */
function storeUser(){
    const firstName = $('#firstName').val();
    const lastName = $('#lastName').val();
    const newRegEmail = $('#newRegEmail').val();
    const newRegPass = $('#newRegPass').val();

    $.ajax({
        url: 'view/admin/userReg.php',
        type: 'POST',
        data: {
            firstName: firstName,
            lastName: lastName,
            newRegEmail: newRegEmail,
            newRegPass: newRegPass
        },
        success: function (data){
            if(data) {
                if (data === "success"){
                    $("#reg_message").empty();
                    $("#reg_message").html("<pre>You've successfully registerd yourself!<br>Please login.</pre>");
                    $("#reg_message").css("color", "green");
                    $("#reg_message").css("display", "block");
                    setTimeout(function(){
                        $("#reg_message").empty();
                        $("#reg_message").css("display", "none");
                        $("#reg_form").css("display", "none");
                        $("#login_form").css("display", "none");
                    }, 2000);
                    $("#nav_login_button").css("display", "block");
                } else {
                    $("#reg_message").empty();
                    $("#reg_message").html(data);
                    $("#reg_message").css("color", "red");
                    $("#reg_message").css("display", "block");
                    setTimeout(function(){
                        $("#reg_message").empty();
                        $("#reg_message").css("display", "none");
                    }, 2000);
                }
            }
        }
    })
}

/**
 * @desc logs in users into the system
 * @since 1.0.0
 */
function login(){
    $("#login_success").css("display", "block");
    setTimeout(function(){
        $("#login_success").css("display", "none");
    }, 2000);

    const userEmail = $('#userEmail').val();
    const userPass = $('#userPass').val();

    $.ajax({
        url: 'view/admin/userLogin.php',
        type: 'POST',
        data: {
            userEmail: userEmail,
            userPass: userPass
        },
        dataType: 'json',
        success: function (data){
            if(data) {
                if (data['status'] === 'loggedin'){
                    $("#login_message").empty();
                    $("#login_message").html("<pre style='color: darkgreen'>Welcome!<br>You've successfully logged in!</pre>");
                    $("#login_message").css("display", "block");
                    setTimeout(function(){
                        $("#login_message").empty();
                        $("#login_message").css("display", "none");
                        window.location.assign("./");
                    }, 2000);
                } else {
                    $("#login_message").empty();
                    $("#login_message").html("<pre style='color: darkred'>Wrong Information Given!!</pre>");
                    $("#login_message").css("display", "block");
                    setTimeout(function(){
                        $("#login_message").empty();
                        $("#login_message").css("display", "none");
                    }, 2000);
                }
            }
        }
    })
}

/**
 * @desc displays login form
 * @since 1.0.0
 */
function displayLoginForm(){
    $("#reg_form").css("display", "none");
    $("#nav_login_button").css("display", "none");
    $("#login_form").css("display", "block");
}

/**
 * @desc displays registration form
 * @since 1.0.0
 */
function subRegBtn(){
    $("#nav_login_button").css("display", "none");
    $("#login_form").css("display", "none");
    $("#reg_form").css("display", "block");
}

/**
 * @desc closes login/registration form
 * @since 1.0.0
 */
function closeLoginRegForm(){
    $("#reg_form").css("display", "none");
    $("#login_form").css("display", "none");
    $("#nav_login_button").css("display", "block");
}

/**
 * @desc displays the form for adding items
 * @since 2.0.0
 */
function displayAddItemsForm(){
    $("#viewItems").css("display", "none");
    $("#viewUsers").css("display", "none");
    $("#add_items_form").css("display", "block");
}

/**
 * @desc hides the form for adding items
 * @since 2.0.0
 */
function closeAddItemsForm(){
    $("#add_items_form").css("display", "none");

}

/**
 * @desc stores new items into the database
 * @since 2.0.0
 */
function storeItems(){
    const productName = $('#productName').val();
    const productCode = $('#productCode').val();
    const productPrice = $('#productPrice').val();
    const productGroup = $('#productGroup').val();
    $.ajax({
        url: 'view/admin/storeItems.php',
        type: 'POST',
        data: {
            productName: productName,
            productCode: productCode,
            productPrice: productPrice,
            productGroup: productGroup
        },
        dataType: 'json',
        success: function (data){
            if(data) {
                if (data['status'] === 'success'){
                    $("#items_added_successful").css("display", "block");
                    setTimeout(function(){
                        $("#items_added_successful").css("display", "none");
                    }, 1500);
                }
            }
        }
    })
}

/**
 * @desc: Loads medicine items and display.
 * @since 2.0.0
 */
function loadItems(){
    $("#add_items_form").css("display", "none");
    $("#viewUsers").css("display", "none");
    $.ajax({
        url: 'view/admin/getItems.php',
        success: function (data){
            if(data) {
                $("#viewItems").css("display", "block");
                $("#viewItems").html(data);
            }
        }
    })
}

/**
 * @desc: Loads users and display.
 * @since 2.0.0
 */
function loadUsers(){
    $("#add_items_form").css("display", "none");
    $("#viewItems").css("display", "none");
    $.ajax({
        url: 'view/admin/getUsers.php',
        success: function (data){
            if(data) {
                $("#viewUsers").css("display", "block");
                $("#usersTable").html(data);
            }
        }
    })
}

/**
 * @desc deletes a specific product
 * item from database
 * @param productID
 * @since 3.0.0
 */
function deleteItem(productID){
    const res = confirm("Are you sure that you want to delete the item?")

    if (res){
        $.ajax({
            url: 'view/admin/deleteItem.php',
            type: 'POST',
            data: {productID: productID},
            success: function (data){
                if(data) {
                    $("#viewItems").css("display", "block");
                    $("#viewItems").html(data);
                }
            }
        })
    }
}

/**
 * @desc updates a specific product
 * item from database
 * @param productID
 * @since 3.0.0
 */
function updateItem(productID){
    const res = confirm("Are you sure that you want to update the item info?")

    if (res){
        const productName = $('#updateProductName'+productID).val();
        const productCode = $('#updateProductCode'+productID).val();
        const productPrice = $('#updateProductPrice'+productID).val();
        const productGroup = $('#updateProductGroup'+productID).val();

        $.ajax({
            url: 'view/admin/updateItem.php',
            type: 'POST',
            data: {
                productID: productID,
                productName: productName,
                productCode: productCode,
                productPrice: productPrice,
                productGroup: productGroup
            },
            success: function (data){
                if(data) {
                    $("#viewItems").css("display", "block");
                    $("#viewItems").html(data);
                }
            }
        })
    }
}

/**
 * @desc deletes a specific product
 * item from database
 * @param productID
 * @since 3.0.0
 */
function deleteUser(userID, email){
    const res = confirm("Are you sure that you want to delete the user (" + email + ") ?");

    if (res){
        $.ajax({
            url: 'view/admin/deleteUser.php',
            type: 'POST',
            data: {userID: userID},
            success: function (data){
                if(data) {
                    $("#viewUsers").css("display", "block");
                    $("#usersTable").html(data);
                }
            }
        })
    }
}

/**
 * @desc updates a specific product
 * item from database
 * @param productID
 * @since 3.0.0
 */
function updateUser(userID, email){
    const res = confirm("Are you sure that you want to update the user (" + email + ") ?")

    if (res){
        const firstName = $('#updateFirstName'+userID).val();
        const lastName = $('#updateLastName'+userID).val();
        const role = $('#updateRole'+userID).val();

        $.ajax({
            url: 'view/admin/updateUser.php',
            type: 'POST',
            data: {
                userID: userID,
                firstName: firstName,
                lastName: lastName,
                role: role
            },
            success: function (data){
                if(data) {
                    $("#viewUsers").css("display", "block");
                    $("#usersTable").html(data);
                }
            }
        })
    }
}