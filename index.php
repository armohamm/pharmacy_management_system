<?php
require_once ('view/include/header.php');
session_start();
?>
<div id="navBarDisplay">
    <?php
    if (isset($_SESSION) && isset($_SESSION['currentUser'])){
        if (isset($_SESSION['currentUser']['status']) && $_SESSION['currentUser']['status'] != "") {
            if ($_SESSION['currentUser']['status'] == "loggedin" && ($_SESSION['currentUser']['role'] == "admin" || $_SESSION['currentUser']['role'] == "moderator")) {
                include_once('view/user/front_end/dashboard.php');
            } else {
                include_once('view/include/upNavbar.php');
            }
        } else {
            include_once('view/include/upNavbar.php');
        }
    } else {
        include_once('view/include/upNavbar.php');
    }
    ?>
</div>
<div class="container">
    <?php include_once('view/user/front_end/login_form.php');?>
    <?php include_once('view/user/front_end/reg_form.php');?>
    <?php
    if ($_SESSION['currentUser']['role'] == "" || $_SESSION['currentUser']['role'] == "user" || !isset($_SESSION['currentUser']['role']))
        include_once('view/user/front_end/getItems.php');
    ?>
</div>
<?php require_once ('view/include/footer.php');?>