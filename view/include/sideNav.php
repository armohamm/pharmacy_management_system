<?php
if (isset($_SESSION['currentUser']) and $_SESSION['currentUser'] != ""): ?>
    <nav id="sidebar">
        <div class="img bg-wrap text-center py-4" style="background-image: url('assets/img/background/bg_1.jpg');">
            <div class="user-logo">
                <div class="img" style="background-image: url('assets/img/icon/logo.jpg');"></div>
                <h3><?php echo $_SESSION['currentUser']['firstName'].' '.$_SESSION['currentUser']['lastName']?></h3>
                <h6><?php echo ucwords($_SESSION['currentUser']['role'])?></h6>
            </div>
        </div>

        <ul class="list-unstyled components mb-5">
            <li class="active">
                <a href="./"><span class="fas fa-chart-line mr-3"></span> Dashboard</a>
            <li>
                <a id="view_users"><span class="fas fa-users mr-3"></span> Users</a>
            </li>
            <li>
                <a id="add_items_btn"><span class="fa fa-plus-circle mr-3"></span> Add Items</a>
            </li>
            <li>
                <a id="view_items"><span class="fa fa-sitemap mr-3"></span> Items</a>
            </li>
            <li>
                <a href="view/admin/userLogout.php" id=""><span class="fa fa-sign-out-alt mr-3"></span> Sign Out</a>
            </li>
        </ul>
    </nav>
<?php endif; ?>