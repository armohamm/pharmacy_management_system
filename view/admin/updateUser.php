<?php
include_once ('../../src/Users.php');

$object = new Users();
$object->set($_POST);
$res = $object->updateUser($_POST['userID']);

if ($res === true)
    $users = $object->getUsers();

$index = 0;

session_start();
if (isset($_SESSION['currentUser']) && $_SESSION['currentUser'] != ""){
    $userRole = $_SESSION['currentUser']['role'];
    $currentUserEmail = $_SESSION['currentUser']['email'];
}

ob_start();
foreach ($users as $user): ?>
    <tr>
        <?php
        $userID = $user['userID'];
        $email = $user['email'];
        ?>
        <?php if ($userRole == "admin"){ ?>
            <th scope="row"><?php echo ++$index;?></th>
            <td><input type="text" class="updateInputFields" value="<?php echo $user['firstName']; ?>" id="updateFirstName<?php echo $userID?>"> <input class="updateInputFields" type="text" value="<?php echo $user['lastName']; ?>" id="updateLastName"></td>
            <td><input disabled type="text" class="updateInputFields" value="<?php echo $user['email']; ?>" id="updateEmail<?php echo $userID?>"></td>
            <td>
                <select class="updateInputFields" id="updateRole<?php echo $userID?>">
                    <option selected value="admin"><?php echo ucwords($user['role']); ?></option>
                    <option value="admin">Admin</option>
                    <option value="moderator">Moderator</option>
                    <option value="user">User</option>
                </select>
            </td>
            <td>
                <button class="button_reset" onclick="updateUser(<?php echo $userID; ?>, <?php echo "'".$email."'"; ?>)" style="color: green">Update</button>
                <?php if ($user['email'] != $currentUserEmail): ?>
                    | <button class="button_reset" onclick="deleteUser(<?php echo $userID; ?>, <?php echo "'".$email."'"; ?>)" style="color: red">Delete</button>
                <?php endif;?>
            </td>
        <?php } else{?>
            <th scope="row"><?php echo ++$index;?></th>
            <td><?php echo $user['firstName'].' '.$user['lastName'];?></td>
            <td><?php echo $user['email'];?></td>
            <td><?php echo $user['role'];?></td>
        <?php }?>
    </tr>
<?php
endforeach;
$contents = ob_get_contents();
ob_end_clean();
echo $contents;
?>