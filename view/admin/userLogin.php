<?php
if (empty($_POST)){
    header('location: ../../');
}
else{
    include_once ('../../src/Users.php');

    $email = $_POST['userEmail'];
    $pass = md5($_POST['userPass']);

    $object = new Users();

    $user = $object->login($email, $pass);

//    print_r($user);

    if (!empty($user)){
        $user["status"] = "loggedin";
        session_start();
        $_SESSION['currentUser'] = $user;
        echo json_encode($user);
        die();
    } else{
        $user["status"] = "failed";
        echo json_encode($user);
        die();
    }
}