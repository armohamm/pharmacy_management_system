<?php
include_once ('../../src/Items.php');

$object = new Items();
$res = $object->delItems($_POST['productID']);

if ($res === true)
    $items = $object->getItems();

ob_start();

echo "<h1 style='margin: 10px'>Medicine List</h1>";
echo "<div class='row'>";

foreach ($items as $item){
    ?>
    <div class="col-sm-6">
        <div style="border: 2px solid #32373d; border-radius: 5px; padding: 10px; margin: 10px">
            <div class="row">
                <div class="col-sm">
                    <h5>Name</h5>
                    <input type="text" value="<?php echo $item['productName'];?>" id="updateProductName">
                </div>
                <div class="col-sm">
                    <h5>Group</h5>
                    <input type="text" value="<?php echo $item['productGroup'];?>" id="updateProductGroup">
                </div>
            </div>
            <div class="row">
                <div class="col-sm">
                    <h5>Code</h5>
                    <input type="text" value="<?php echo $item['productCode'];?>" id="updateProductCode">
                </div>
                <div class="col-sm">
                    <h5>Price</h5>
                    <input type="text" value="<?php echo $item['productPrice'];?>" id="updateProductPrice">
                </div>
            </div>
            <div class="row">
                <button class="btn btn-success" id="updateBtn" onclick="updateItem(<?php echo $item['productID']; ?>)" style="margin: 10px">Update</button>
                <button class="btn btn-danger" id="deleteBtn" onclick="deleteItem(<?php echo $item['productID']; ?>)" style="margin: 10px">Delete</button>
            </div>
        </div>
    </div>
    <?php
}
echo "</div>";
$contents = ob_get_contents();
ob_end_clean();
echo $contents;
?>