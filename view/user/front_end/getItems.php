<?php
include_once ('src/Items.php');

$object = new Items();
$items = $object->getItems();
$index = 0;
?>
<h1 style='margin: 10px'>Medicine List</h1>
<div class='row'>
<?php foreach ($items as $item): ?>
    <div class="col-sm-6">
        <div style="border: 2px solid #32373d; border-radius: 5px; padding: 10px; margin: 10px">
            <div class="row">
                <div class="col-sm">
                    <h5>Name</h5>
                    <?php echo $item['productName'];?>
                </div>
                <div class="col-sm">
                    <h5>Group</h5>
                    <?php echo $item['productGroup'];?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm">
                    <h5>Code</h5>
                    <?php echo $item['productCode'];?>
                </div>
                <div class="col-sm">
                    <h5>Price</h5>
                    <?php echo $item['productPrice'];?>
                </div>
            </div>
            <div class="row">
                <button class="btn btn-success" style="margin: 10px 10px 10px 15px">Buy</button>
            s</div>
        </div>
    </div>
<?php endforeach; ?>
</div>
