<div id="login_form"
<!-- Default form login-->
    <form class="text-center border border-light p-5 login-form">

        <p class="h4 mb-4">Sign in</p>
        <div id="login_message"></div>
        <input required type="email" id="userEmail" name="userEmail" class="form-control mb-4" placeholder="E-mail">
        <input required type="password" id="userPass" name="userPass" class="form-control mb-4" placeholder="Password">

        <div class="d-flex justify-content-around">
            <div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
                    <label class="custom-control-label" for="defaultLoginFormRemember">Remember me</label>
                </div>
            </div>
            <div>
                <a href="">Forgot password?</a>
            </div>
        </div>

<!--        <button id="sign_me_button" class="btn btn-info btn-block my-4">Sign in</button>-->
        <button id="sign_me_button" class="btn btn-info btn-block my-4">Sign in</button>

        <p>Not a member?
            <button class="login_button_reset" id="sub_reg_button">Register</button>
        </p>
        <p><button class="close_button" id="sub_close_button_login_form">Close</button></p>

        <p>or sign in with:</p>

        <a href="#" class="mx-2" role="button"><i class="fab fa-facebook-f light-blue-text"></i></a>
        <a href="#" class="mx-2" role="button"><i class="fab fa-twitter light-blue-text"></i></a>
        <a href="#" class="mx-2" role="button"><i class="fab fa-linkedin-in light-blue-text"></i></a>
        <a href="#" class="mx-2" role="button"><i class="fab fa-github light-blue-text"></i></a>

    </form>
<!-- Default form login-->
</div>