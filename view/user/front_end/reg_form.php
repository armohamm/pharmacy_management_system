<div id="reg_form">
    <!-- Default form login-->
    <form class="text-center border border-light p-5 login-form" action="#!">

        <p class="h4 mb-4">Sign Up</p>
        <div id="reg_message"></div>
        <div class="row">
            <div class="col-md-6">
                <input required type="text" id="firstName" name="firstName" class="form-control mb-4" placeholder="First Name">
            </div>
            <div class="col-md-6">
                <input required type="text" id="lastName" name="lastName" class="form-control mb-4" placeholder="Last Name">
            </div>
        </div>

        <input required type="email" id="newRegEmail" name="newRegEmail" class="form-control mb-4" placeholder="E-mail">
        <input required type="password" id="newRegPass" name="newRegPass" class="form-control mb-4" placeholder="Password">

        <button class="btn btn-info btn-block my-4" id="reg_me_button">Register Me!</button>

        <p>Already a member?
            <button class="reg_button_reset" id="sub_login_button">Sign in</button>
        </p>

        <p><button class="close_button" id="sub_close_button_reg_form">Close</button></p>

        <p>or sign in with:</p>

        <a href="#" class="mx-2" role="button"><i class="fab fa-facebook-f light-blue-text"></i></a>
        <a href="#" class="mx-2" role="button"><i class="fab fa-twitter light-blue-text"></i></a>
        <a href="#" class="mx-2" role="button"><i class="fab fa-linkedin-in light-blue-text"></i></a>
        <a href="#" class="mx-2" role="button"><i class="fab fa-github light-blue-text"></i></a>
    </form>
    <!-- Default form login-->
</div>