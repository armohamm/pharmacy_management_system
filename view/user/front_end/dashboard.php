<?php
if (isset($_SESSION['currentUser']) and $_SESSION['currentUser'] != ""){
    $userRole = $_SESSION['currentUser']['role'];
?>
    <div id="sideNavDisplay">
        <div class="wrapper d-flex align-items-stretch">
            <?php include_once ('view/include/sideNav.php');?>
            <!-- Page Content  -->
            <div id="content" class="p-4 p-md-5 pt-5">
                <div class="container">
                    <div id="add_items_form">
                        <?php include_once('add_items_form.php');?>
                    </div>

                    <div id="viewItems"></div>

                    <div id="viewUsers">
                        <table class="table table-dark">
                            <h1>Members List</h1>
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Full Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Role</th>
                                <?php if ($userRole == "admin"): ?>
                                <th scope="col">Action</th>
                                <?php endif; ?>
                            </tr>
                            </thead>
                            <tbody id="usersTable">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>