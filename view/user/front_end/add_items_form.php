<!-- Default form login-->
<form class="text-center border border-light p-5 login-form" action="#!">
    <div class="row">
        <div class="col-md-12">
            <p class="h4 mb-4">Add Items</p>
            <div id="items_added_successful">
                <pre>New Item Added!</pre>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <input required type="text" id="productName" name="productName" class="form-control mb-4" placeholder="Product Name">
                </div>
                <div class="col-md-6">
                    <input required type="text" id="productCode" name="productCode" class="form-control mb-4" placeholder="Product Code">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <input required type="text" id="productPrice" name="productPrice" class="form-control mb-4" placeholder="Product Price">
                </div>
                <div class="col-md-6">
                    <input required type="text" id="productGroup" name="productGroup" class="form-control mb-4" placeholder="Product Group">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-info btn-block my-4" id="add_item_btn">Add Item!</a>
                </div>
            </div>

            <p>
                <a class="close_button" id="sub_close_button_add_item_form">Close</a>
            </p>
        </div>
    </div>
</form>
<!-- Default form login-->